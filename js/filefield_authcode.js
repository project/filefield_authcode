/**
 * @file
 * Provides JavaScript tweaks for password protected fields.
 *
 */

(function ($) {

  /**
 * Attach behavior.
 */
  Drupal.behaviors.fieldfieldAuthcode = {
    attach: function (context, settings) {
      if (settings.filefieldAuthcode.disabledURIs) {
        $.each(settings.filefieldAuthcode.disabledURIs, function(index, value) {
         $('a[href='+ value  +']').click(function(e){
           return false;
         });
        });
      }
    }
  };

})(jQuery);
